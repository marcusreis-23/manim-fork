from manimlib import *
from manimlib.mobject.graph_theory import *

class GraphTheoryExample(Scene):
    def construct(self):
       #es = [ (0,1),(1,2),(2,0) ]
       #poss = [ 2*UP,3*LEFT,2*DOWN ]
       #graph = Graph(edge_set=es,pos_arr=poss,name_fn=ALPHABET_NAMES,directed=True,size=0.6)

       graph = KGraph(8,ALPHABET_NAMES,size=0.1)
       self.play( ShowCreation(graph) )
