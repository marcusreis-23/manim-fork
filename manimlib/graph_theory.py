from manimlib.constants import *
from manimlib.mobject.geometry import Circle
from manimlib.mobject.geometry import Line
from manimlib.mobject.geometry import Arrow
from manimlib.mobject.geometry import TipableVMobject
from manimlib.mobject.types.vectorized_mobject import VGroup
from manimlib.mobject.svg.tex_mobject import TexText
from manimlib.utils.config_ops import digest_config
from math import sin,cos,floor
from string import ascii_lowercase

NUMBER_NAMES = lambda n: str(n)
VOID_NAMES = lambda n: None
# does not account for graphs with more than 26 nodes (yet)
ALPHABET_NAMES = lambda n: list(ascii_lowercase)[n] 

class GraphNode(Circle): 
    CONFIG = {
        "highlighted": False,
    }
    # TODO: point nodes maybe?
    def __init__(self, pos, name=None, size=0.4, is_target=False, **kwargs):
        digest_config(self, kwargs)
        super().__init__(radius=size, **kwargs)
        self.set_stroke(width=size*20)
        self.move_to(pos)
        self.position = pos
        self.is_target = is_target
        # not necessary to generate an entire mobject if name is not specified
        if name:
            name_text = TexText(str(name))
            name_text.scale(size*2)
            name_text.move_to(pos)
            self.add(name_text)
    #def highlight(self, color=GREEN_SCREEN):
    #def unhighlight(self): is this an actual word?

class GraphEdge(Line):
    CONFIG = {
        "highlighted": False,
        "directed": False,
        "curved": False,
    }
    def __init__(self, start, end, size=0.4, arrow=False, **kwargs):
        digest_config(self, kwargs)
        Line.__init__(self, start=start, end=end, **kwargs)
        if arrow:
            self.add_tip()
        self.set_stroke(color=GREY_A, width=20*size)
    #def highlight(self, color=GREEN_SCREEN):
    #def unhighlight(self): is this an actual word?

class Graph(VGroup):
    def __init__(self, edge_set, pos_arr, directed=False, size=0.4, 
            name_fn=VOID_NAMES, **kwargs):
        self.number = len(pos_arr)
        self.size = size
        self.directed = directed
        super().__init__(**kwargs)
       
        self.add( VGroup( *self.gen_nodes(name_fn,pos_arr) ) )
        self.add( VGroup( *self.gen_edges(edge_set,pos_arr) ) )

    def gen_nodes(self, name_fn, positions):
        vertices = VGroup()
        for i in range(self.number):
            vertices.add(GraphNode(positions[i],name=name_fn(i),size=self.size))
        return vertices

    def gen_edges(self, edge_set, n_pos):
        edges = VGroup()
        for node1,node2 in edge_set:
            edges.add(self.connect_nodes( (n_pos[node1],n_pos[node2]) ))
        return edges
    def connect_nodes(self, node_pair):
        # TODO: 
        # - automatic edge curving if node is in the way or multiple edges for the 
        # same 2 nodes (I don't think there's an elegant way of doing this...)
        line = Line( node_pair[0], node_pair[1] )
        unit_vector = line.get_unit_vector()
        start, end = line.get_start_and_end()
            
        constant_scale_value = 1.12
        
        # edge should not go to center of node, but rather to its border
        new_start = start + unit_vector*self.size*constant_scale_value
        new_end = end - unit_vector*self.size*constant_scale_value
        # for some reason arrow tips are a bit longer than the actual line
        if self.directed:
            new_end = new_end - unit_vector*self.size*0.18

        edge = GraphEdge( new_start, new_end, size=self.size, arrow=self.directed )

        return edge
    
    #def bfs(self, node_search, multiple=False):
    #def dfs(self, node_search, multiple=False):
    #def remove_node(self, node):
    #def remove_edge(self, edge):
    #def color_solve(self, number):
    #def subdivide_graph(self):
    #def node_degree(self, node):
    #def isomorphism_to(self, graph):

class KGraph(Graph):
    def __init__(self, number, name_fn=VOID_NAMES, size=0.4, **kwargs):
        self.n = number
        Graph.__init__(self, edge_set=self.get_all_edges(), 
                pos_arr=self.get_nodes_positions(size), name_fn=name_fn, **kwargs)
    def get_nodes_positions(self,scale):
        nodes_positions = []
        # auto scales big graphs (not ideal yet)
        r = 20*scale if self.n < 9 else self.n*scale*2.5
        for i in range(self.n):
            nodes_positions.append( RIGHT*r*cos(2*PI*i/self.n)+UP*r*sin(2*PI*i/self.n) )
        return nodes_positions
    def get_all_edges(self):
        edges = []
        for i in range(self.n-1):
            for j in range(i+1,self.n):
                edges.append( (i,j) )
        return edges

#class BipartiteGraph(Graph):
#class KBipartiteGraph(BipartiteGraph):
